import argparse

parser = argparse.ArgumentParser(description='Script to run the VAE model')
parser.add_argument('--n_max_training_steps', help='number of training steps', default=10000)
parser.add_argument('--batch_size', help='batch size used during training', default=500)
parser.add_argument('--lr',help='Learning rate',default=0.001)
parser.add_argument('--z_dim',help='Latent space dimension', default=8)
parser.add_argument('--hiddenlayers',help='Number of layers. Input and output layers are always in the network, 1 hidden layer in minimum.', default=2)
parser.add_argument('--hiddendim',help='Hidden dimension',default=256)

parser.add_argument('--seed', help='Seed for random weight initialization', default=1)
parser.add_argument('--n_train', help='Training set size', default=-1)
parser.add_argument('--save_class_4', help='Save results of class 4 (produces large files)', default=0)
parser.add_argument('--output', help='Output to file (give filename) or to "console"', default="out.txt")
args = parser.parse_args()

# Hyperparameters
z_dim = int(args.z_dim)
lr = float(args.lr)
prefix = 'seeds'
n_max_training_steps = int(args.n_max_training_steps)
batch_size = int(args.batch_size)
hiddenlayers = int(args.hiddenlayers)
hiddendim = int(args.hiddendim)
hidden_dim = hiddenlayers*[hiddendim]
n_train = int(args.n_train)
seed = int(args.seed)
save_class4 = int(args.save_class_4)
folder = "{}_vae_ntrain_{}_nmaxtrainsteps_{}_batchsize_{}_zdim_{}_hiddenlayers_{}_hiddendim_{}_lr_{}_seed_{}".format(\
        prefix, n_train, n_max_training_steps, batch_size, z_dim, hiddenlayers, hiddendim, lr, seed)

input_dim = 8
w_kl = 0.1

settings_current_model = [ (x, str(eval(x))) for x in dir() if not "__" in x]

from dataloader import Dataloader_elpasolites
from dataloader import generate_n_valid_samples, get_noise
import os, sys, json, time
import torch
from torch import nn
import numpy as np

time_start = time.time()

if not os.path.isdir(folder):
    os.mkdir(folder)
    os.chdir(folder)
    print(f"Now running {folder}.")
else:
    print(f"Run {folder} already exists. Exiting gracefully")
    sys.exit(0)

with open("settings.json", "w") as out:
    json.dump( settings_current_model  , out )

if str(args.output)!="console": sys.stdout = open(str(args.output),"w")

os.environ["CUDA_VISIBLE_DEVICES"]=""
device = torch.device('cpu')
torch.manual_seed(seed)
torch.set_num_threads(4)


# Load dataset
dataset = Dataloader_elpasolites(seed=seed,n_train=n_train, batch_size=batch_size)
n_classes = dataset.n_classes
data_train = np.array(dataset.descriptor_train_scaled,dtype=float)
labels_train = np.array(dataset.classes_formation_energies)


class VAE(nn.Module):
    def __init__(self,
                 hidden_dim,
                 z_dim,
                 n_classes,
                 input_dim
                 ):
        super(VAE, self).__init__()
        
        # Encoder layers
        modules = [nn.Sequential(
                    nn.Linear(input_dim + n_classes , hidden_dim[0]),
                    nn.LayerNorm(hidden_dim[0]),
                    nn.LeakyReLU(0.2)
                    )]
        for i in range(len(hidden_dim)-1):
            modules.append(nn.Sequential(
                            nn.Linear(hidden_dim[i] , hidden_dim[i+1]),
                            nn.LayerNorm(hidden_dim[i+1]),
                            nn.LeakyReLU(0.2)
                            ))
        self.enc = nn.Sequential(*modules)
        self.out_mean = nn.Linear(hidden_dim[-1],z_dim)
        self.out_logvar = nn.Linear(hidden_dim[-1],z_dim) 

        # Decoder layers
        hidden_dim.reverse()
        modules = [nn.Sequential(
                    nn.Linear(z_dim + n_classes , hidden_dim[0]),
                    nn.LayerNorm(hidden_dim[0]),
                    nn.LeakyReLU(0.2)
                    )]
        for i in range(len(hidden_dim)-1):
            modules.append(nn.Sequential(
                            nn.Linear(hidden_dim[i] , hidden_dim[i+1]),
                            nn.LayerNorm(hidden_dim[i+1]),
                            nn.LeakyReLU(0.2)
                            ))
        self.dec = nn.Sequential(*modules)
        self.out = nn.Linear(hidden_dim[-1], input_dim)

    def encode(self, x, c):
        # Encode the input given a class label and return mean and log variance
        x = torch.cat((x, c), dim=-1)
        h_enc = self.enc(x) 
        return self.out_mean(h_enc) , self.out_logvar(h_enc)

    def sampling(self, z_mean, z_logvar):
        # Sample from latent space using reparametrization trick
        epsilon = torch.randn_like(z_logvar)
        return torch.exp(0.5 * z_logvar) * epsilon + z_mean

    def decode(self, z):
        # Decode a sample from the latent space given a class label
        h_dec = self.dec(z)
        return torch.tanh(self.out(h_dec))

    def forward(self, x, c):
        z_mean, z_logvar = self.encode(x, c)
        z = self.sampling(z_mean, z_logvar)
        z_c = torch.cat((z, c), dim=-1)        
        return self.decode(z_c) , z_mean, z_logvar

def vae_loss(x_decoded_mean, x , z_mean, z_logvar):
    mse = nn.MSELoss(reduction='none')
    mse_loss = torch.mean(torch.sum(mse(x_decoded_mean.view(-1,input_dim), x.view(-1,input_dim)),dim=1),dim=0)
    kl_loss = w_kl*torch.mean(-0.5 * torch.sum(1 + z_logvar - z_mean.pow(2) - z_logvar.exp(),dim=1),dim=0)
    return mse_loss + kl_loss

model = VAE(hidden_dim,z_dim,n_classes,input_dim).to(device)
optimizer = torch.optim.Adam(model.parameters(),lr=lr)
print('Model architectures')
print(model)
print("\n\n Starting VAE training\n\n")

training_steps=0
model.train()
for epoch in range(1000000): # arbitrary number, we stop according to update steps
    train_loss = 0
    N = 0
    for batch_idx, (data,labels) in enumerate(dataset.train_loader(data_train, labels_train, \
                                        construct_class_balanced=True, drop_last=True,batch_size=dataset.batch_size)):
        training_steps+=1
        if training_steps >= n_max_training_steps: break
        data = data.float()
        labels = labels.float()
        optimizer.zero_grad()
        output, mean, logvar = model(data,labels)
        loss = vae_loss(output, data , mean, logvar)
        loss.backward()
        train_loss += loss.item()*data.size(0)
        N += data.size(0)
        optimizer.step()
        train_loss = train_loss/N
    print('Epoch: {}\ttrain loss: {}'.format(epoch,train_loss))
    if training_steps >= n_max_training_steps: break
    if str(args.output)!="console": sys.stdout.flush()


print("Updates", training_steps)
print(f"Time for training: {time.time()-time_start} s")

# Saving models
torch.save(VAE, 'VAE.pth')
print("\nFinished VAE training")

model.eval()

print("----------------------------------")
print("\nEvaluating conditional generation for class 0 (valid)")

class_to_generate = 0
valid_samples_to_generate = 10000
generated_valid,generated_invalid,generated_all = generate_n_valid_samples(model=model.decode, dataset=dataset, n_target=valid_samples_to_generate, n_iter=1000, z_dim=z_dim, class_label=class_to_generate, unique=False)
if generated_valid is not None:
    dataset.evaluate_generation(generated_valid, 'chemical_formula',\
        tag='class0_valid_10000')
else:
    print("generation of requested number of 10.000 valid samples failed, badly trained model")
    sys.exit(1)
sys.stdout.flush()

valid_samples_to_generate = 250000
generated_valid,generated_invalid,generated_all = generate_n_valid_samples(model=model.decode, dataset=dataset, \
        n_target=valid_samples_to_generate, n_iter=2500, z_dim=z_dim, class_label=class_to_generate, unique=False)
if generated_valid is not None:
    dataset.evaluate_generation(generated_valid, 'chemical_formula',\
        tag='class0_valid')
else:
    print("generation of requested number of 250.000 valid samples failed, badly trained model")
    sys.exit(1)
sys.stdout.flush()

if int(args.save_class_4):
    print("----------------------------------")
    print("\nEvaluating conditional generation for class 4")
    class_to_generate = 4
    valid_samples_to_generate = 2500000
    generated_valid,generated_invalid,generated_all = generate_n_valid_samples(model=model.decode, dataset=dataset, \
        n_target=valid_samples_to_generate, n_iter=25000, z_dim=z_dim, class_label=class_to_generate, unique=False)
    dataset.evaluate_generation(generated_valid, 'chemical_formula',\
        tag='class4_valid_2.5mio',save=int(args.save_class_4))
    
    valid_samples_to_generate = 25000000
    generated_valid,generated_invalid,generated_all = generate_n_valid_samples(model=model.decode, dataset=dataset, \
        n_target=valid_samples_to_generate, n_iter=25000, z_dim=z_dim, class_label=class_to_generate, unique=False)
    dataset.evaluate_generation(generated_valid, 'chemical_formula',\
        tag='class4_valid',save=int(args.save_class_4))

print("Time passed: {}".format(time.time()-time_start))
sys.stdout.flush()

