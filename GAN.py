import argparse
parser = argparse.ArgumentParser( description='Script to run the wGAN-GP model.' )
#parser.add_argument('--n_max_training_steps', help='number of training steps', default=200000)
#parser.add_argument('--batch_size', help='batch size used during training', default=500)
#parser.add_argument('--lr', help='Learning rate for discriminator (critic) and generator network.', default=0.00001)
#parser.add_argument('--z_dim', help='Latent space dimension', default=64)
#parser.add_argument('--hiddenlayers', help='Number of layers. Input and output layers are always in the network, setting it to 0 means only including those.', default=6)
#parser.add_argument('--hiddendim', help='Hidden dimension', default=128)

#parser.add_argument('--seed', help='Seed for random weight initialization', default=1)
#parser.add_argument('--n_train', help='Training set size', default=-1)
#parser.add_argument('--save_class_4', help='Save results of class 4 (produces large files)', default=0)
#parser.add_argument('--output', help='Output to file (give filename) or to "console"', default="out.txt")
#parser.add_argument('--use_gpu', help='Use the GPU', default=0)


## remove this
parser.add_argument('--n_max_training_steps', help='number of training steps', default=200000)
parser.add_argument('--batch_size', help='batch size used during training', default=100)
parser.add_argument('--lr', help='Learning rate for discriminator (critic) and generator network.', default=0.00001)
parser.add_argument('--z_dim', help='Latent space dimension', default=8)
parser.add_argument('--hiddenlayers', help='Number of layers. Input and output layers are always in the network, 1 hidden layer is minimum', default=1)
parser.add_argument('--hiddendim', help='Hidden dimension', default=512)

parser.add_argument('--seed', help='Seed for random weight initialization', default=1)
parser.add_argument('--n_train', help='Training set size', default=-1)
parser.add_argument('--save_class_4', help='Save results of class 4 (produces large files)', default=0)
parser.add_argument('--output', help='Output to file (give filename) or to "console"', default="out.txt")
parser.add_argument('--use_gpu', help='Use the GPU', default=0)


args = parser.parse_args()

# Hyperparameters
z_dim = int(args.z_dim)
lr = float(args.lr)
prefix = "seeds" # Prefix for folder
crit_update_steps = 5 # Critic update steps in each training iteration
n_max_training_steps = int(args.n_max_training_steps)
lambda_penalty = 10 # Gradient penalty factor
batch_size = int(args.batch_size)
hiddenlayers = int(args.hiddenlayers)
hiddendim = int(args.hiddendim)
n_train = int(args.n_train)
seed = int(args.seed)
save_class4 = int(args.save_class_4)
use_gpu = int(args.use_gpu)
folder = "{}_gan_ntrain_{}_nmaxtrainsteps_{}_batchsize_{}_zdim_{}_hiddenlayers_{}_hiddendim_{}_lr_{}_seed_{}".format(prefix, 
                                                                                             n_train, n_max_training_steps, 
                                                                                             batch_size, z_dim, 
                                                                                             hiddenlayers, hiddendim, lr, seed)


hidden_dim = [int(args.hiddendim)] + [int(args.hiddendim)]*(hiddenlayers-1) + [int(args.hiddendim)]
settings_current_model = [ (x, str(eval(x))) for x in dir() if not "__" in x]

import pickle, os, sys, json, time
from copy import deepcopy
import torch
from torch import nn
from torch.autograd import grad
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from dataloader import Dataloader_elpasolites
from dataloader import generate_n_valid_samples, get_noise 

time_start = time.time()

if not os.path.isdir(folder): 
    os.mkdir(folder)
    os.chdir(folder)
    print(f"Now running {folder}.")
else:
    print(f"Run {folder} already exists. Exiting gracefully")
    sys.exit(0)

with open("settings.json", "w") as out:
    json.dump( settings_current_model  , out )

activation_g = nn.LeakyReLU(0.2)
activation_d = nn.LeakyReLU(0.2)

# Change folder and download data
if str(args.output)!="console": sys.stdout = open(str(args.output),"w")

torch.use_deterministic_algorithms(True)
torch.manual_seed(seed)
if use_gpu:
    device = torch.device("cuda")
    torch.backends.cudnn.benchmark = False
else:
    device = torch.device("cpu")
    torch.set_num_threads(8)

# Load dataset
dataset = Dataloader_elpasolites( n_train = n_train, seed=seed, batch_size = batch_size, folder="../paper_data" )
n_classes = dataset.n_classes

class Critic(nn.Module):
    def __init__(self):
        global n_classes, hidden_dim, hiddenlayers
        input_dim_c = 8 + n_classes
        super(Critic, self).__init__()
        
        i = 0
        layers = [nn.Linear(input_dim_c, hidden_dim[0]), activation_d]
        for i in range(hiddenlayers-1):
            layers.append( nn.Linear(hidden_dim[i], hidden_dim[i+1]) )
            layers.append( activation_d )
        layers.append( nn.Linear(hidden_dim[i+1], 1) )
        self.crit = nn.Sequential( *layers )
    
    def forward(self, image):
        return self.crit(image)

class Generator(nn.Module):
    def __init__(self):
        global zdim, n_classes, hidden_dim, hiddenlayers
        input_dim_c = z_dim + n_classes
        super(Generator, self).__init__() 
       
        i = 0
        layers = [nn.Linear(input_dim_c, hidden_dim[0]), activation_g]
        for i in range(hiddenlayers-1):
            layers.append( nn.Linear(hidden_dim[i], hidden_dim[i+1]) )
            layers.append( activation_d )
        layers.append( nn.Linear(hidden_dim[i+1], 8) )
        self.generate = nn.Sequential( *layers )

    def forward(self, noise): 
        x = self.generate(noise)
        return nn.Tanh()(x)


def get_rand_category(n_samples):
    global n_classes, device
    randint = torch.randint( 0, n_classes, (n_samples,) )
    return torch.nn.functional.one_hot(randint, n_classes).float()        

gen = Generator().to(device)
crit = Critic().to(device)
gen_opt = torch.optim.Adam(gen.parameters(), lr=lr, betas=(0.5, 0.9))
crit_opt = torch.optim.Adam(crit.parameters(), lr=lr, betas=(0.5, 0.9))

remove_class1=False
if remove_class1:
    desc = dataset.descriptor_train_scaled
    Ef = dataset.classes_formation_energies
    desc = [x for i,x in enumerate(desc) if not Ef[i]==0] 
    Ef = [x for i,x in enumerate(Ef) if not Ef[i]==0] 
    train_loader = dataset.train_loader(desc, Ef, device=device)
else:
    train_loader = dataset.train_loader(dataset.descriptor_train_scaled, dataset.classes_formation_energies, device=device)


print('Model architectures')
print(gen)
print(crit)
print("\n\n Starting WGAN-GP training\n\n")

updates=0 #
w_estimate = []; c_loss=[]; c_loss_real=[]; training_steps=0; g_loss=[]; gp_loss=[]
gen.train(); crit.train()
for epoch in range(1000000): # arbitrary number, we stop according to update steps, could be a while loop
    crit_loss_av = 0.; gen_loss_av = 0.; crit_loss_real_av = 0.; gp_loss_av=0

    for i, batch in enumerate(train_loader):

        training_steps+=1
        if training_steps > n_max_training_steps: break

        for j in range(crit_update_steps):

          # Get real data for batch
          real, real_category = batch
          real = torch.cat([real.float(), real_category.float()],1)

          # Get fake data for batch
          fake_noise = get_noise(len(real_category), z_dim=z_dim).to(device)
          rand_category = get_rand_category(len(real_category)).to(device)
          fake_gen = gen( torch.cat((fake_noise, rand_category),1) )
          fake = torch.cat( (fake_gen, rand_category), 1 )

          # update crit
          crit_fake_pred = crit(fake.detach()) 
          crit_real_pred = crit(real)
          ad_fake_loss = torch.mean(crit_fake_pred)    
          ad_true_loss = -torch.mean(crit_real_pred)   

          ###### Gradient penalty
          # gradient penalty
          alpha = torch.rand(real.size()[0], 1).expand( real.size() ).to(device)
          one = torch.tensor([1]).to(device)
          x_hat = alpha * real + (one - alpha) * fake
          pred_hat = crit(x_hat)
          gradients = grad( outputs=pred_hat, inputs=x_hat, 
                            grad_outputs=torch.ones(pred_hat.size()).to(device),
                            create_graph=True, retain_graph=True, only_inputs=True)[0]
          gradient_penalty = lambda_penalty * ((gradients.view(gradients.size()[0], -1).norm(2, 1) - 1) ** 2).mean()
          ############

          crit_opt.zero_grad();
          ( (ad_true_loss + ad_fake_loss + gradient_penalty) ).backward(retain_graph=True)
          crit_opt.step()

        ### Update generator ###
        fake_noise = get_noise( len(real_category), z_dim=z_dim).to(device)
        rand_category = get_rand_category( len(real_category) ).to(device)
        fake_noise = torch.cat( (fake_noise, rand_category), 1)
        fake_gen = gen(fake_noise)
        fake = torch.cat( (fake_gen, rand_category), 1 )

        # Gen loss
        crit_fake_pred = crit(fake)
        loss_G = -torch.mean(crit_fake_pred) # max fake score
        gen_opt.zero_grad()
        loss_G.backward()
        updates+=1
        gen_opt.step()
        
        # logging of losses
        crit_loss_av += ( float( torch.mean(crit_fake_pred) ) - float( torch.mean(crit_real_pred) ) ) + float( gradient_penalty )
        gen_loss_av += float( -loss_G )
        crit_loss_real_av += float( torch.mean(crit_real_pred) )
        gp_loss_av += float( gradient_penalty )

    c_loss += [crit_loss_av/(i+1)]
    c_loss_real += [crit_loss_real_av/(i+1)]
    w_estimate += [ (crit_loss_real_av - gen_loss_av) / (i+1)]
    gp_loss += [gp_loss_av/(i+1)]
    g_loss += [gen_loss_av/(i+1)]
    print( "Epoch: {0}, Step: {1}, c: {2:2.2f}".format(epoch, training_steps, crit_loss_av) )
    if training_steps >= n_max_training_steps: break
    if str(args.output)!="console": sys.stdout.flush() 

print("Updates", updates)
print(f"Time for training: {time.time()-time_start} s")

# Saving models
torch.save(gen, "gen.pth")
torch.save(crit, "crit.pth")
print("\nFinished WGAN-GP training")

# Losses
plt.plot(c_loss, label="Crit loss GP")
plt.plot(c_loss_real, label="Crit loss real")
plt.plot(g_loss, label="Crit loss fake")
plt.plot(w_estimate, label="Wasserstein estimate")
plt.plot(gp_loss, label="GP loss")
plt.ylabel("Loss", fontsize=15)
plt.xlabel("Epoch", fontsize=15)
plt.legend(fontsize=15)
plt.savefig("gan_losses.pdf")
plt.show()

gen.eval(); crit.eval(); gen.zero_grad()
print("----------------------------------")
print("\nEvaluating conditional generation for class 0 (valid)")

generated_valid, generated_invalid, generated_all = generate_n_valid_samples( gen, dataset, n_target=10000, n_iter=2500, n_max_iter=1000, class_label = 0, z_dim=z_dim, device=str(device) )
if generated_valid is not None: # not None
    dataset.evaluate_generation(generated_valid, inputtype="chemical_formula", tag='class0_valid_10000')
else: 
    print("generation of requested number of 10.000 valid samples failed, badly trained model")
    sys.exit(1)
sys.stdout.flush()

generated_valid, generated_invalid, generated_all = generate_n_valid_samples( gen, dataset, n_target=25000, n_iter=2500, n_max_iter=2500, class_label = 0, z_dim=z_dim, device=str(device) )
if generated_valid is not None: 
    dataset.evaluate_generation(generated_valid, inputtype="chemical_formula", tag='class0_valid_25000')
else:
    print("generation of requested number of 25.000 valid samples failed, badly trained model")
    sys.exit(1)
sys.stdout.flush()

generated_valid, generated_invalid, generated_all = generate_n_valid_samples( gen, dataset, n_target=250000, n_iter=2500, n_max_iter=25000, class_label = 0, z_dim=z_dim, device=str(device) )
if generated_valid is not None:
    dataset.evaluate_generation(generated_valid, inputtype="chemical_formula", tag='class0_valid')
else:
    print("generation of requested number of 250.000 valid samples failed, badly trained model")
    sys.exit(1)
sys.stdout.flush()

if int(args.save_class_4):
    print("----------------------------------")
    print("\nEvaluating conditional generation for class 4")
    gen_opt.zero_grad()
    generated_valid, generated_invalid, generated_all = generate_n_valid_samples( gen, dataset, n_target=2500000, n_iter=25000, class_label = 4, z_dim=z_dim, device=str(device) )
    dataset.evaluate_generation(generated_valid, inputtype="chemical_formula", tag='class4_valid_2.5mio', save=int(args.save_class_4))
    generated_valid, generated_invalid, generated_all = generate_n_valid_samples( gen, dataset, n_target=25000000, n_iter=25000, class_label = 4, z_dim=z_dim, device=str(device) )
    dataset.evaluate_generation(generated_valid, inputtype="chemical_formula", tag='class4_valid', save=int(args.save_class_4))

print("Time passed: {}".format(time.time()-time_start))
sys.stdout.flush()
