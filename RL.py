# -*- coding: utf-8 -*-
import argparse

parser = argparse.ArgumentParser( description='Script to run the RL model.' )
parser.add_argument('--n_max_training_steps', help='Number of training steps', default=800000)
parser.add_argument('--batch_size', help='Batch size used during training', default=250)
parser.add_argument('--lr', help='Learning rate for the network', default=1e-05)
parser.add_argument('--hiddenlayers', help='Number of layers. Input layers are not counted, so one layer (the output layer) is the minimal network size.', default=5)
parser.add_argument('--hiddendim', help='Hidden dimension', default=512)

parser.add_argument('--seed', help='Seed for random weight initialization', default=999)
parser.add_argument('--n_train', help='Training set size. -1 corresponds to the full permuted set, -2 to the training set by Faber et al.', default=-1)
parser.add_argument('--beta', help='Base of the softmax function for the action probabilities', default=5)
parser.add_argument('--output', help='Output to file (give filename) or to "console"', default="out.txt")
args = parser.parse_args()

# Hyperparameters
n_max_training_steps = int(args.n_max_training_steps)
batch_size = int(args.batch_size)
n_train = int(args.n_train)
seed = int(args.seed)
lr = float(args.lr)
hidden_layers=int(args.hiddenlayers)
hiddendim = int(args.hiddendim)
beta=float(args.beta)

folder = "rl_ntrain_{}_nmaxtrainsteps_{}_batchsize_{}_hiddenlayers_{}_hiddendim_{}_lr_{}_seed_{}".format(n_train, n_max_training_steps, 
                                                                                             batch_size, hidden_layers, hiddendim, lr, seed)
hidden_dim = [hiddendim]*(hidden_layers)


import torch
import torch.nn as nn
import random
import numpy as np
from dataloader import Dataloader_elpasolites 
import os

import time
import sys
from scipy.special import softmax


class Environment():
    ''' Defines the actions the agent can take dependent on the given input state
    '''

    def __init__(self):
        super().__init__() 
  
    def get_legal_actions(self, s):
        tochoose=list(s[0]).index(0.)
        if tochoose%2==0:
            legalactions=range(1,7)
            sl=[(2*i-1)/6.-1 for i in legalactions]
        else:
            legalactions=range(1,9)
            sl=[(2*i-1)/8.-1 for i in legalactions]
        return legalactions, sl

class Agent():
    ''' Agent that moves through the environment by taking actions.
    '''

    def __init__(self):
        ''' Agent initialization
        '''     
        super().__init__()

    def select_action(self, h, legalactions, multi, beta):
        ''' Agent selects the next action based on the predictions for a good reward based on the network output as weights for a random action selection.
        '''     
        with torch.no_grad():
            k=policy_net(h)

            k=softmax(k*beta, axis=1)
            if len(legalactions)==6:
                kpick=[random.choices(legalactions, k[i,:-2]) for i in range(len(k))] 

            else:
                kpick=[random.choices(legalactions, k[i]) for i in range(len(k))] 
            return kpick 
 
    def learn(self, data):
        ''' Learning process of the agent for the obtained data batch.
        '''
        state, next_state, action, reward, done, idx, c = data
        non_final_mask=[True if i==0 else False for i in done]
        non_final_next_states = torch.cat([s for i,s in enumerate(next_state)
			if done[i] == 0.])
        non_final_next_states=non_final_next_states.reshape((int(len(non_final_next_states)/8),8))
        q=policy_net(state).gather(1, action.unsqueeze(1))
        next_v = torch.zeros(len(state))
        next_v[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
        next_q = ((next_v * GAMMA) + reward).float()
        loss = loss_fn(q, next_q.unsqueeze(1))
        optimizer.zero_grad()

        loss.backward()
        optimizer.step()

        return loss.mean().detach()


class NeuralNetwork(nn.Module):
    ''' Contains the Neural Network. Architecture is build according to input parameters.
    '''
    def __init__(self):
        ''' Network initialization and setup of architecture.
        '''     
        super(NeuralNetwork, self).__init__()

        modules = [nn.Sequential(
                    nn.Linear(input_dim , hidden_dim[0]),
                    nn.LayerNorm(hidden_dim[0]),
                    nn.LeakyReLU(0.2),
                    )]
        for i in range(len(hidden_dim)-1):
            modules.append(nn.Sequential(
                            nn.Linear(hidden_dim[i] , hidden_dim[i+1]),
                            nn.LayerNorm(hidden_dim[i+1]),
                            nn.LeakyReLU(0.2),
                            ))
        modules.append(nn.Linear(hidden_dim[-1],output_dim))
        self.linear_stack = nn.Sequential(*modules)

    def forward(self, x):
        ''' Forward pass of input data trough the network. Predicted maximal final rewards for each action are returned.
        '''     
        pred = self.linear_stack(x)
        return pred 

if __name__=='__main__':

    time_start = time.time()
        
    random.seed(seed)
    torch.manual_seed(seed)
    
    #output
    if not os.path.isdir(folder): 
        os.mkdir(folder)
        os.chdir(folder)
        print(f"Now running {folder}.")
    else:
        print(f"Run {folder} already exists. Exiting gracefully")
        sys.exit(0)
    
    if str(args.output)!="console": sys.stdout = open(str(args.output),"w")



    #Dataloader
    dataset = Dataloader_elpasolites(folder='../paper_data',batch_size=batch_size, n_train=n_train, seed=seed, reinforcement_learning=True, faber_set=True)
    labels_train = np.array(dataset.classes_formation_energies)
    formulas_all = np.array(dataset.chemical_formulas_all)

    #settings for generation
    n_classes = dataset.n_classes
    valid_samples_to_generate = 10000
    class_to_generate = 0

    c=[0]*n_classes
    c[class_to_generate]=1
    c=torch.tensor(c, dtype=torch.float32)

    #Generation and loading of the trainingset
    dataset.create_rl_trainingset(class_to_generate)
    data_train = dataset.train_loader([dataset.state_memory, dataset.next_state_memory, dataset.action_memory, dataset.reward_memory, dataset.done_memory, dataset.idx], dataset.energy_memory, reinforcement_learning=True, priority_sampling=True)

    #set up network  
    input_dim = 8
    output_dim=8
    GAMMA = 0.999
    TARGET_UPDATE=10

    agent=Agent()
    env=Environment()
    policy_net=NeuralNetwork()
    target_net=NeuralNetwork()
    target_net.load_state_dict(policy_net.state_dict())
    target_net.eval()

#    #load network (if necessary)   
#    policy_net=torch.load('rl_{}.pt'.format(seed))
#    target_net=torch.load('rl_{}.pt'.format(seed))

    #Training process
    loss_fn = torch.nn.MSELoss()  
    optimizer = torch.optim.Adam(policy_net.parameters(), lr=lr)

    alllosses=[]    
    training_steps=0
    learn=True
    while learn==True:
        if training_steps %10==0:
            print('------training step {}-------'.format(training_steps))

        for j, i in enumerate(data_train):
            loss=agent.learn(i)
            alllosses.append(loss)
            training_steps+=1
            if training_steps > n_max_training_steps: learn=False
            if training_steps % TARGET_UPDATE == 0:
                target_net.load_state_dict(policy_net.state_dict())

    np.save('loss', alllosses)
    torch.save(policy_net, 'rl_{}.pt'.format(seed))
    
    #Generation
    count_valid=0 #number of generated samples that are returning a valid structure
    count_total=0 #total number of generation attempts
    max_tries=2000000 #number of structure generation attempts (includes invalid structures) before the generation stops
    multi=10000 #number of structures generated in one forward pass trough the network
    generated_valid = [] #stores valid generated structures
    print('-----Begin Prediction for beta={}-----'.format(beta))
    while count_valid<valid_samples_to_generate:
        w=torch.zeros(size=(multi,8))
        for j in range(0,8):
            legalactions, sl=env.get_legal_actions(w)
            action=agent.select_action(w, sl, multi,beta)    
            w[:,j]=torch.from_numpy(np.array(action).T) 
        generated = [np.rint(dataset.unscale(x.detach().numpy().reshape(4,2))).astype(int) for x in w]

        for x in generated:
            count_total+=1
            if count_valid<valid_samples_to_generate and dataset._check_valid(x)==True:
                generated_valid.append(x)
                count_valid+=1
        if count_total>max_tries:
            count_valid=max_tries

    print('Requested class {}'.format(class_to_generate))
    evaluation=dataset.evaluate_generation(generated_valid, \
            tag='class{}_valid_{}'.format(class_to_generate,valid_samples_to_generate)) 

    print("Time passed: {}".format(time.time()-time_start))
    sys.stdout.flush()    

