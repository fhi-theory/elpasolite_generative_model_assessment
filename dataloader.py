import pickle, os, re, json, time
import numpy as np
import pandas as pd
import torch
import scipy
import seaborn as sns
import matplotlib.pyplot as plt
from copy import copy, deepcopy
from collections import Counter
from itertools import combinations 
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

class Dataloader_elpasolites():
    """ Class to handle the elpasolite dataset in various representations 

    Attributes
    ----------
    batch_size: integer
            Number of training set samples in each batch used in the gradient descent
    n_classes: integer
            Number of formation energy classes of equal span
    permutation: boolean
            Doubles the training set by permuting A/B atoms and assigning the same energy to the 2 equivalent structures
    identfiers: dictionary
            Maps row and group in the periodic table to the corresponding chemical element
    descriptor_train: array
            Contains for each element in the structure the corresponding row and group in the periodic table
    descriptor_train_scaled: array
            Contains for each element in the structure the corresponding row and group in the periodic table scaled between (-1,1)
    chemical_formula_train: string
            Representation of the training set structures as chemical formula ABC2D6
    formation_energies_train: array
            Formation energy in eV for each structure in the training set
    chemical_formulas_all: string
            Representation of the structures of the full dataset as chemical formula ABC2D6
    formation_energies_train: array
            Formation energy in eV for each structure in the full dataset
    """
    
    def __init__(self, folder="paper_data", seed=42, n_train=-1, batch_size=100, reinforcement_learning=False):
        """ Function that loads the Trainingset and sets up attributes. 
        Folder should contain allEnergies.txt and TrainingSet.pkl
        seed is used when forming training sets of different sizes. Set a fixed seed to make results reproducible
        n_train sets the size of the training set (will be multiplied x2 if permutation=True) 
        n_train=-1 refers to the original training set by Faber et al.  without duplicate structures, n_train=-2 refers to the complete original training set """
        
        self.batch_size = batch_size
        self.n_classes = 10
        self.permutation = True
        self.descriptor_test=[]

        # Store identifiers for conversion
        self.identifiers = { '1-1':"H", '1-8':"He", 
                   '2-1':"Li", '2-2':"Be", '2-3':"B", '2-4':"C", '2-5':"N", '2-6':"O", '2-7':"F", '2-8':"Ne", 
                   '3-1':"Na", '3-2':"Mg", '3-3':"Al", '3-4':"Si", '3-5':"P", '3-6':"S", '3-7':"Cl", '3-8':"Ar", 
                   '4-1':"K", '4-2':"Ca", '4-3':"Ga", '4-4':"Ge", '4-5':"As", '4-6':"Se", '4-7':"Br", '4-8':"Kr", 
                   '5-1':"Rb", '5-2':"Sr", '5-3':"In", '5-4':"Sn", '5-5':"Sb", '5-6':"Te", '5-7':"I", '5-8':"Xe", 
                   '6-1':"Cs", '6-2':"Ba", '6-3':"Tl", '6-4':"Pb", '6-5':"Bi", "6-6": "Po", "6-7": "At", "6-8":"Rn" }
        self.inv_identifiers = {v: k for k, v in self.identifiers.items()}

        # 1) Load trainingset
        with open(os.path.join(folder, "TrainingSet.pkl"), 'rb') as f:
            u = pickle._Unpickler(f)
            u.encoding = 'latin1'
            p = u.load()
            #  The original training set includes 10590 compositions and formation_energies and looks like this:
            #  p["Co"][0]  # relative positions (10,3)
            #  p["Ce"][0]  # Unit cell (3,3)
            #  p["N"][0]   # Number of atoms 
            #  p["T"][0]   # formation energy in eV 
            #  p["X"][0]   # Descriptor: For each site 1-4 the row in the PSE and the number of valence electrons (4,2)
            #  p["Z"][0]   # Atomic charge (10,)
 
        # The training set 
        self.descriptor_train = p["X"]
        self.formation_energies_train = p["T"] / 10. # to eV/atom
        print("Original training set size:", len(self.descriptor_train))

        if n_train>-2:
            # The training set contains 34 duplicates (they are occuring in the permuted form)
            # We are removing the second occurring one here, reducing the training set size from 10590 to 10556
            chemical_formula_train_nonpermuted = [self.convert(x, "descriptor", "chemical_formula") for x in self.descriptor_train ]
            index_remove = []
            index_keep = []
            for x in self.descriptor_train:
                y = self._permute_ab(x)
                form1 = self.convert(y, "descriptor", "chemical_formula")
                form2 = self.convert(x, "descriptor", "chemical_formula")
                if  form1 in chemical_formula_train_nonpermuted and\
                    form2 in chemical_formula_train_nonpermuted: 
                    index1 = chemical_formula_train_nonpermuted.index(form1)
                    index2 = chemical_formula_train_nonpermuted.index(form2)    
                    index_remove.append(max([index1, index2]))   
                    index_keep.append(min([index1, index2]))
            self.descriptor_train = [x for i,x in enumerate(self.descriptor_train) if not i in index_remove]
            self.descriptor_train_dropped = [x for i,x in enumerate(self.descriptor_train) if i in index_remove]
            self.formation_energies_train_dropped = [self.formation_energies_train[i] for i in index_remove] 
            self.formation_energies_train_kept = [self.formation_energies_train[i] for i in index_keep] 
            self.formation_energies_train = [x for i,x in enumerate(self.formation_energies_train) if not i in index_remove] 
            print("Training set size after removal of duplicate cases:", len(self.descriptor_train))
    
            # reduced training set size?
            # the following is implemented: 
            # A training set of size 2000 fully contains the respective smaller size training subset of say size 1000
            if n_train>0:
                self.formation_energies_full_trainingset = self.formation_energies_train
                idxs = list(range(len(self.descriptor_train)))
                np.random.seed(seed)
                np.random.shuffle(idxs)
                idxs_train = idxs[:n_train]
                self.descriptor_train = [x for i,x in enumerate(self.descriptor_train) if i in idxs_train]
                self.formation_energies_train = [x for i,x in enumerate(self.formation_energies_train) if i in idxs_train]            
                print("Training set size after reduction:", len(self.formation_energies_train))
        else:
            self.permutation=False
                
        # Load descriptor and formation energies
        if self.permutation == True:
            self.descriptor_train_perm = np.empty((len( self.descriptor_train )*2,4,2), dtype=int)
            self.formation_energies_train_perm = np.empty(len( self.formation_energies_train )*2, dtype=float) 
            for i in range(len(self.descriptor_train)):
                self.descriptor_train_perm[i] = self.descriptor_train[i]
                self.descriptor_train_perm[i+len(self.descriptor_train)] = self._permute_ab(self.descriptor_train[i])
                self.formation_energies_train_perm[i]=self.formation_energies_train[i]
                self.formation_energies_train_perm[i+len(self.formation_energies_train)] = self.formation_energies_train[i]
            self.descriptor_train = self.descriptor_train_perm
            self.descriptor_train_perm = 0
            self.formation_energies_train = self.formation_energies_train_perm
            self.formation_energies_train_perm = 0 
            
            if n_train > 0:
                self.formation_energies_full_trainingset_perm = np.empty(len( self.formation_energies_full_trainingset )*2, dtype=float) 
                for i in range(len(self.formation_energies_full_trainingset)):
                    self.formation_energies_full_trainingset_perm[i]=self.formation_energies_full_trainingset[i]
                    self.formation_energies_full_trainingset_perm[i+len(self.formation_energies_full_trainingset)] = self.formation_energies_full_trainingset[i]
                self.formation_energies_full_trainingset = self.formation_energies_full_trainingset_perm
                self.formation_energies_full_trainingset_perm = 0 

            print("Training set size after permutation:", len( self.descriptor_train ))

            # duplicate removal 
            self.descriptor_train, index = np.unique(self.descriptor_train.reshape(-1,8),axis=0,return_index=True)
            self.descriptor_train = self.descriptor_train.reshape(-1,4,2).astype(int)
            self.formation_energies_train = np.take(self.formation_energies_train,index,axis=0)
            print("Training set size after duplicate removal:", len( self.descriptor_train ))

        # convert to other representations
        self.descriptor_train_scaled = np.array([self._scale(x) for x in self.descriptor_train])
        self.chemical_formula_train = [self.convert(x, "descriptor", "chemical_formula") for x in self.descriptor_train]

        # 2) Load all energies and chemical formulas
        self.chemical_formulas_all = [x.split()[0] for x in \
                                      open(os.path.join(folder, "allEnergies.txt")).readlines()]
        self.formation_energies_all = [float(x.split()[1]) for x in \
                                  open(os.path.join(folder, "allEnergies.txt")).readlines()]
    
        # Get formation energies and classes
        self.classes_formation_energies,self.bins_energy = self._categorize_energy_onehot(self.formation_energies_train, self.n_classes)
        if n_train>0:
            self.bins_energy = self._categorize_energy_onehot(self.formation_energies_full_trainingset, self.n_classes)[1]
            self.classes_formation_energies = self._categorize_energy_onehot(self.formation_energies_train, self.bins_energy)[0]
        self.descriptor_train_scaled = np.array([self._scale(x) for x in self.descriptor_train]).reshape(-1,8)

        self.set_all = set( self.chemical_formulas_all )
        self.set_train = set( self.chemical_formula_train )

        if reinforcement_learning:
            num=len(self.descriptor_train_scaled)*8
            self.state_memory=np.empty((num,8))
            self.next_state_memory=np.empty((num,8))
            self.action_memory=np.empty(num, dtype=int)
            self.reward_memory=np.empty(num)
            self.energy_memory=np.empty((num,self.n_classes))
            self.done_memory=np.empty(num)
            self.prio=np.ones(num)*1000
            self.idx=np.arange(0,num,1)

    def _scale(self, rep):
        """ Scales the period (group) to the range [-1,1] by dividing it in 6 (8) 
        intervals of the same size and taking the middle point of each interval """
        newrep = np.empty((4,2))
        newrep[:,0] = (2*rep[:,0]-1)/6 -1
        newrep[:,1] = (2*rep[:,1]-1)/8 -1
        return newrep

    def unscale(self, rep):
        """ Inverse of _scale """
        newrep=np.empty((4,2))
        newrep[:,0] = ((rep[:,0]+1)*6+1)/2
        newrep[:,1] = ((rep[:,1]+1)*8+1)/2
        return newrep	

    def train_loader(self, data_train_x, data_train_y, device="cpu", shuffle=True, 
                     drop_last=False, construct_class_balanced=True, batch_size=None, reinforcement_learning=False, priority_sampling=False):
        """ Constructs a torch Dataloader that can be iterated 
            data_train_x: structure representation
            data_train_y: energy label
            shuffle: randomize training set before forming batches
            drop_last: remove last batch when it's of a smaller size than batch_size
            construct_class_balanced: weighted sampling with weights inverse of the size of the corresponding class"""

        if batch_size==None:
            batch_size=self.batch_size
        if reinforcement_learning:
            data_train = torch.utils.data.TensorDataset(torch.tensor(data_train_x[0]).float().to(device) , torch.tensor(data_train_x[1]).float().to(device) , 
                                torch.tensor(data_train_x[2]).to(device) , torch.tensor(data_train_x[3]).to(device) , torch.tensor(data_train_x[4]).to(device) , torch.tensor(data_train_x[5]).to(device) ,
                                                    torch.tensor(data_train_y).to(device))
        else:
            data_train = torch.utils.data.TensorDataset(torch.tensor(data_train_x).float().to(device) , 
                                                    torch.tensor(data_train_y).to(device))
        if construct_class_balanced:
            classes = list(np.argmax(data_train_y, 1)) # one-hot encoded classes 
            class_labels = list(set(classes))
            weight = np.array([1./classes.count(x) for x in class_labels])
            sampling_weight = weight[classes]
            trainloader = torch.utils.data.DataLoader( data_train, batch_size=batch_size,
                                                       shuffle=False, drop_last=drop_last,\
                                            sampler=torch.utils.data.sampler.WeightedRandomSampler(sampling_weight,len(sampling_weight)) )
        else:
            trainloader = torch.utils.data.DataLoader(data_train, batch_size=batch_size,\
                                           shuffle=shuffle, drop_last=drop_last, sampler=None)
        if priority_sampling:
            sampling_weight = self.prio
            trainloader = torch.utils.data.DataLoader( data_train, batch_size=batch_size,
                                                       shuffle=False, drop_last=drop_last,\
                                            sampler=torch.utils.data.sampler.WeightedRandomSampler(sampling_weight,len(sampling_weight)) )
        return trainloader

    def _permute_ab(self, ab_r):
        """ Function that permutes position A und B of an representation"""
        ba_r=[ab_r[1], ab_r[0]]
        for i in ab_r[2:]: ba_r.append(i)
        return np.array(ba_r)

    def _check_valid(self, sample, inputtype="descriptor"):
        """ Function that checks if sample coresponds to a valid chemical formula"""
        try:
            sample = self.convert(sample, inputtype, "chemical_formula")
            if not sample in self.set_all:
               return False
        except:
            return False

        return True

    ##########################
    ### Conversion methods between structural represenations
    def convert(self, inp, input_type="chemical_formula", output_type="descriptor", standardize_permutation=True):
        
        """ Function to handle interconversion among representations
        Please only use this function, not the helpers!
        
        Possible choices for interconversion
        ------------------------------------
        descriptor : Same as in training set of elpasolites data
            For each site 1-4 the row in the PSE and the number of valence electrons (4,2)
        chemical_formula : Simple chemical formula
        elements : List of elements used on sites 1-4
        """
        
        if input_type == output_type: return inp

        # convert to chemical formula as standard rep
        if input_type=="descriptor":
            chemical_formula = self._descriptor_to_chemical_formula(inp)
        elif input_type=="elements":
            chemical_formula = "{}{}{}2{}6".format(*list(inp))
        else:
            chemical_formula = inp

        # convert to desired output
        if output_type=="descriptor":
            output = self._chemical_formula_to_trainingsetrep(chemical_formula)
        elif output_type=="elements":
            output = self._get_elements_from_trainingsetrep(self._chemical_formula_to_trainingsetrep(chemical_formula))
        else:
            output = chemical_formula
            
        return output

    def _categorize_energy_onehot(self, energy, bins):
        """ Function that divides the formation energies in one-hot encoded classes"""

        if isinstance(bins,int):
            nbins = bins
        else:
            nbins = len(bins)-1
        category, bins_classes =pd.cut(energy,nbins,retbins=True,labels=range(nbins))
        energy_onehot=[]
        for b in category:
            onehot=np.zeros((nbins))
            onehot[b]=1
            energy_onehot.append(onehot)
        return energy_onehot,bins_classes

    def _descriptor_to_chemical_formula(self, representation):
        """Function that converts the descriptor representation to a chemical formula """
            
        chemical_formula = ""; elements = []
        for i,rep in enumerate(representation):
            ident = "{}-{}".format(rep[0],rep[1])
            chemical_formula += self.identifiers[ident]
            if i==2: chemical_formula+="2"
            if i==3: chemical_formula+="6"
        return chemical_formula

    def _chemical_formula_to_trainingsetrep(self, formula):
        """ Function that takes the chemical formula and encodes it into the descriptor representation
                 For each site 1-4 the row in the PSE and the number of valence electrons (4,2)
        """
        formula_back = formula

        if formula[1].islower(): element1 = formula[0:2]; formula = formula[2:]
        else: element1 = formula[0]; formula = formula[1:]

        if formula[1].islower(): element2 = formula[0:2]; formula = formula[2:]
        else: element2 = formula[0]; formula = formula[1:]

        if formula[1].islower(): element3 = formula[0:2]; formula = formula[3:] # stochiometry 2
        else: element3 = formula[0]; formula = formula[2:]

        if formula[1].islower(): element4 = formula[0:2];
        else: element4 = formula[0]

        rep = [[int(x) for x in self.inv_identifiers[element1].split("-")]]
        rep += [[int(x) for x in self.inv_identifiers[element2].split("-")]]
        rep += [[int(x) for x in self.inv_identifiers[element3].split("-")]]
        rep += [[int(x) for x in self.inv_identifiers[element4].split("-")]]

        return rep
    
    def _trainingsetrep_to_onehot(self, rep):
        ''' Function that takes the descriptor representation (x vector[valence electrons, main group #]) 
        and returns the elements in one hot encoded form e.g. in form [[0 0 0 0 1 0 0 0], [1 0 0 0 0 0 0 0],..,[0 0 0 0 0 0 1 0]]'''
        rep = np.array(rep).flatten()
        onehot = np.zeros((8,8))
        for i,r in enumerate(rep):
            onehot[i,r-1]=1
        return onehot

    def _get_elements_from_trainingsetrep(self,r):
        ''' Function that takes the representation (x vector[valence electrons, main group #]) 
        and returns the elements for that composition e.g. in form ['Al', 'Na', 'K', 'F']'''

        periodictable_rep=np.array([['H', '' , '', '', '', '', '', 'He'],
                                    ['Li','Be','B','C','N','O','F','Ne'],
                                    ['Na','Mg','Al','Si','P','S','Cl','Ar'],
                                    ['K','Ca','Ga','Ge','As','Se','Br','Kr'],
                                    ['Rb','Sr','In','Sn','Sb','Te','I','Xe'],
                                    ['Cs','Ba','Tl','Pb','Bi','','','']])
        r = np.array(r)
        q=[]
        for j in range(len(r)):
            w=periodictable_rep[r[j,0]-1][r[j,1]-1]
            if len(w)==0:
                raise Exception('This representation does not map to a specific element.')
            else:
                q.append(w)
        return q

    #########################################
    # Methods that apply to reinforcement learning
    def create_rl_trainingset(self, targetclass):
        """ This function splits every datapoint in the trainingset into 8 individual decisions and pushes them to the cache memory of the agent"""  
        missing_list=list(combinations(range(0, 8), 8))
        emptyc=torch.tensor(np.zeros(self.n_classes))

        for i, rep in enumerate(self.descriptor_train_scaled):
            for missing in missing_list:
                c=self.classes_formation_energies[i]
                #reward r definition
                if targetclass==list(c).index(1):
                    r=1
                else:
                    r=-1

                self._add_all_choices_to_cache( rep, missing, r, c, self.formation_energies_train[i], i)


    def _add_all_choices_to_cache(self, b, k, r, c, energy, pos):
        """ This function processes the data in the form that it can be saved in the cache memory for RL learning""" 
        b=b.flatten()
        k=sorted(k)
        e=b.copy()
        d=b.copy()

        #reward
        for i in range(len(k)):
            for j in range(i+1): 
                e[k[len(k)-j-1]]=0
                if j!=i: 
                    d[k[len(k)-j-1]]=0
           
            pred=d[k[len(k)-j-1]]
            new_s=[n for n in d]
            old_s=[n for n in e]
            if k[len(k)-j-1]%2==0: 
                action=np.around(((pred+1)*6+1)*0.5 -1) #-1 because we want list indices not the actual value
            else:
                action=np.around(((pred+1)*8+1)*0.5 -1)
            if 0. not in new_s:
                done=1
            else:
                done=0 

            self._cache(old_s, new_s, int(action), r, c, pos*8+i, done)    

    def _cache(self, state, next_state, action, reward,  energy, pos, done): #
        """
        Stores the data in the cache memory lists (replay buffer)
        """
        self.state_memory[pos]=state
        self.next_state_memory[pos]=next_state
        self.action_memory[pos]=action
        self.reward_memory[pos]=reward*done
        self.energy_memory[pos]=energy
        self.done_memory[pos]=done


    def _compute_js_divergence(self, formulas_generated, formulas_reference, return_matrices=False):
        """ Function that computes the Jensen-Shannon divergence between a generated distribution and a reference distribution """

        all_elements = list(self.identifiers.values())[0:-3]

        elements_generated = np.array([self.convert(x, "chemical_formula", "elements") \
                              for x in formulas_generated])

        elements_reference = np.array([self.convert(x, "chemical_formula", "elements") \
                              for x in formulas_reference])

        matrix_encoded_generated  = np.zeros((len(all_elements),4))    
        matrix_encoded_generated[:,0] = [list(np.array(elements_generated)[:,0].flatten()).count(x)\
                                         for x in all_elements]
        matrix_encoded_generated[:,1] = [list(np.array(elements_generated)[:,1].flatten()).count(x)\
                                         for x in all_elements]
        matrix_encoded_generated[:,2] = [list(np.array(elements_generated)[:,2].flatten()).count(x)\
                                         for x in all_elements]
        matrix_encoded_generated[:,3] = [list(np.array(elements_generated)[:,3].flatten()).count(x)\
                                         for x in all_elements]

        matrix_encoded_reference  = np.zeros((len(all_elements),4))    
        matrix_encoded_reference[:,0] = [list(np.array(elements_reference)[:,0].flatten()).count(x)\
                                         for x in all_elements]
        matrix_encoded_reference[:,1] = [list(np.array(elements_reference)[:,1].flatten()).count(x)\
                                         for x in all_elements]
        matrix_encoded_reference[:,2] = [list(np.array(elements_reference)[:,2].flatten()).count(x)\
                                         for x in all_elements]
        matrix_encoded_reference[:,3] = [list(np.array(elements_reference)[:,3].flatten()).count(x)\
                                         for x in all_elements]
        
        vals = scipy.spatial.distance.jensenshannon(matrix_encoded_reference, 
                                                    matrix_encoded_generated, base=2)

        for i in range(4):
            matrix_encoded_reference[:,i] /= np.sum( matrix_encoded_reference[:,i] )
            matrix_encoded_generated[:,i] /= np.sum( matrix_encoded_generated[:,i] )
            #matrix_encoded_reference[:,i] += 1e-10
            #matrix_encoded_generated[:,i] += 1e-10
            #m = (matrix_encoded_generated[:,i] + matrix_encoded_reference[:,i]) / 2.0
            #val = scipy.stats.entropy( matrix_encoded_reference[:,i], m )
            #val += scipy.stats.entropy( matrix_encoded_generated[:,i], m )
            #val /= 2.0        
        
        
        divergence = np.mean(np.array(vals)**2)
        distance = np.mean(vals)
        if return_matrices:
            return divergence, distance, vals, matrix_encoded_generated, matrix_encoded_reference
     
        # returns js divergence, js distance, single values for distance
        return divergence, distance, vals


    ###########################
    ### Performance evaluation

    def evaluate_generation(self, newgen_valid, inputtype="descriptor", tag='', save=True):
        """ Implements our standardized evaluation of generative model success for class 0 and 4 
            1) Outputs statistics about generated samples.
                In our nomenclature:
                Total attempts: 
                    How many new samples were actually requested (e.g. 1000)
                Correct: 
                    How many samples were actually found in allEnergies.txt
                    This is further split into Discovery and Rediscovery.
                    - Discovery: Was not contained in allEnergies.txt
                    - Rediscovery: Was contained in allEnergies.txt
                Invalid: 
                    Combines the former categories errorneous and invalid samples in one measure. 
                    Errorneous means, non-existing elements are encoded (e.g. 1-6)
                    Invalid means, a sample is not in the allEnergies.txt 
                    (e.g. a single perovskite, we are only interested in double perovskites)
            2) Conditional evaluation class 0, class 4
            3) Plot of element occurence in generated (valid, unique, true discovery) systems over sites 1-4
            
        """
        
        #### save chemical compositions in the right order to a file
        idx_formulas = {} # lookup dict (fast)
        for i,f in enumerate(self.chemical_formulas_all):
            idx_formulas[f]=i
        chemical_formulas_generated = [self.convert(s, inputtype, "chemical_formula") for s in newgen_valid]
        idx = [ idx_formulas[i] for i in chemical_formulas_generated ]
        formation_energies_generated = [ self.formation_energies_all[idx_formulas[i]]  for i in chemical_formulas_generated ]
        
        if save:
            with open('{}generated.txt'.format(tag), 'w') as f:
                for i in idx:
                    f.write('{}   {}\n'.format(self.chemical_formulas_all[i], self.formation_energies_all[i]))

        #### compute statistics over sampled compositions:
        # novel discoveries (in each class), repetitions, rediscoveries
        results = {f"new_discovery_class{idx}": [0] for idx in range(self.n_classes)}
        results["new_discoveries"] = [0]
        results["repeated_discoveries"] = [0]
        results["rediscoveries_train_unique"] = [0] 
        results["rediscoveries_train"] = [0] 

        encountered_unique = set()
        discovery = []
        rediscovery = []
        for i,chemical_formula in enumerate(chemical_formulas_generated):
            found = chemical_formula in encountered_unique
            if not found: # a novel sample was found
                encountered_unique.update([chemical_formula])
            if not chemical_formula in self.set_train: # the sample was not in the training set
                discovery.append(chemical_formula)
                if not found:
                    results["new_discoveries"].append( results["new_discoveries"][-1]+1 )
                    if self.bins_energy[1]>formation_energies_generated[i]:
                        classx=0
                    elif self.bins_energy[-2]<formation_energies_generated[i]:
                        classx = self.n_classes-1
                    else:
                        classx = [j for j in range(1,len(self.bins_energy)-2,1) if self.bins_energy[j]<formation_energies_generated[i] and formation_energies_generated[i]<=self.bins_energy[j+1] ][0]
                    results[f"new_discovery_class{classx}"].append( results[f"new_discovery_class{classx}"][-1]+1 )
                else: # the sample was already encountered during sampling
                    results["repeated_discoveries"].append( results["repeated_discoveries"][-1]+1 )
            else: # sample was in training set
                rediscovery.append(chemical_formula)
                if not found:
                    results["rediscoveries_train_unique"].append( results["rediscoveries_train_unique"][-1]+1 )
                results["rediscoveries_train"].append( results["rediscoveries_train"][-1]+1 )

            # all elements not in the loop changed remain the same
            for j,k in enumerate(results.keys()):
                if not len(results[k])==i+2:
                    results[k].append(results[k][-1])

        if save: json.dump( results, open(tag+'dict_discovery_rates.json', 'w') )
        print( "\nSupplied (unique) samples: {} ({})".format( len(chemical_formulas_generated), len(set(chemical_formulas_generated)) ) )
        print( "- Discovery: {} ({})".format( len( discovery ), len( set( discovery ) ) ) )        
        print( "- Rediscovery in trainingset: {} ({})".format( len( rediscovery ), len(set( rediscovery )) ) )        


        ############ conditional evaluation class 0, class 4
        # counting how many unique generated samples are in the right class and in the neighboring classes

        # categorize
        compositions_all = { f"class{idx}":[] for idx in range(self.n_classes) }
        compositions_sampled = deepcopy(compositions_all)
        for i,chemical_formula in enumerate(self.chemical_formulas_all):
            if self.bins_energy[1]>self.formation_energies_all[i]:
                classx=0
            elif self.bins_energy[-2]<self.formation_energies_all[i]:
                classx = self.n_classes-1
            else:
                classx = [j for j in range(1,len(self.bins_energy)-2,1) if self.bins_energy[j]<self.formation_energies_all[i] and self.formation_energies_all[i]<=self.bins_energy[j+1] ][0]
            compositions_all[f"class{classx}"].append(chemical_formula)
        for i,chemical_formula in enumerate(chemical_formulas_generated):
            if self.bins_energy[1]>formation_energies_generated[i]:
                classx=0
            elif self.bins_energy[-2]<formation_energies_generated[i]:
                classx = self.n_classes-1
            else:
                classx = [j for j in range(1,len(self.bins_energy)-2,1) if self.bins_energy[j]<formation_energies_generated[i] and formation_energies_generated[i]<=self.bins_energy[j+1] ][0]
            compositions_sampled[f"class{classx}"].append(chemical_formula)
        compositions_train = { f"class{idx}":[] for idx in range(self.n_classes) }
        class_idx_train = np.argmax(self.classes_formation_energies,1)
        for i,chemical_formula in enumerate(self.chemical_formula_train):
            classx = class_idx_train[i]
            compositions_train[f"class{classx}"].append(chemical_formula)

        counts_neighboring_classes_class0 = len( compositions_sampled["class1"] )
        counts_neighboring_classes_class4 = len( compositions_sampled["class3"] ) + len( compositions_sampled["class5"] )
        counts_neighboring_classes_class0_unique = len(set( compositions_sampled["class1"] ))
        counts_neighboring_classes_class4_unique = len(set( compositions_sampled["class3"] )) + len(set( compositions_sampled["class5"] ))

        divergence_class0, distance_class0, dist_single_values0 = self._compute_js_divergence(chemical_formulas_generated, compositions_all["class0"])
        divergence_class4, distance_class4, dist_single_values4 = self._compute_js_divergence(chemical_formulas_generated, compositions_all["class4"])
        divergence_class0_train, distance_class0_train, dist_single_values0_train = self._compute_js_divergence(chemical_formulas_generated, compositions_train["class0"])
        divergence_class4_train, distance_class4_train, dist_single_values4_train = self._compute_js_divergence(chemical_formulas_generated, compositions_train["class4"])

        print("\n-------- CLASS 1 --------")
        print("Members of class 1: {}".format( len(compositions_all["class0"]) ))
        print("Coverage (right class): {} ({:.3f}%)".format( len(set(compositions_sampled["class0"])),\
                    100 * len(set(compositions_sampled["class0"])) / len(compositions_all["class0"]) ))
        print("Coverage (neighboring class): {} ({:.3f}%)".format( counts_neighboring_classes_class0_unique,\
                    100*counts_neighboring_classes_class0_unique/len(compositions_all["class1"]) ))
        print("Precision (right class): {} ({:.3f}% of discoveries)".format( len(compositions_sampled["class0"]),\
                    100*len(compositions_sampled["class0"])/len(chemical_formulas_generated) ) )
        print("Precision (neighboring classes): {} ({:.3f}% of discoveries)".format(counts_neighboring_classes_class0,\
                    100*counts_neighboring_classes_class0/len(chemical_formulas_generated)))
        print("JS-Divergence: {:.3f}".format( divergence_class0 ) )
        print("JS-Distance: {:.3f}".format( distance_class0 ) )
        print(f"Single distances: {dist_single_values0}")
        print("JS-train-divergence: {:.3f}".format( divergence_class0_train ) )
        print("JS-train-distance: {:.3f}".format( distance_class0_train ) )
        print(f"Single train distances: {dist_single_values0_train}")
        print(f"Single train distances: {dist_single_values0_train}")

        print("\n-------- CLASS 5 --------")        
        print("Members of class 5: {}".format( len(compositions_all["class4"]) ))
        print("Coverage (right class): {} ({:.3f}%)".format( len(set(compositions_sampled["class4"])),\
                    100 * len(set(compositions_sampled["class4"])) / len(compositions_all["class4"]) ))
        print("Coverage (neighboring class): {} ({:.3f}%)".format( counts_neighboring_classes_class4_unique,\
                    100 * counts_neighboring_classes_class4_unique / ( len(compositions_all["class3"]) + len(compositions_all["class5"]) ) ))
        print("Precision (right class): {} ({:.3f}% of discoveries)".format( len(compositions_sampled["class4"]),\
                    100*len(compositions_sampled["class4"])/len(chemical_formulas_generated) ))
        print("Precision (neighboring classes): {} ({:.3f}% of discoveries)".format(counts_neighboring_classes_class4,\
                    100*counts_neighboring_classes_class4/len(chemical_formulas_generated)))
        print("JS-Divergence: {:.3f}".format( divergence_class4 ) )
        print("JS-Distance: {:.3f}".format( distance_class4 ) )
        print(f"Single distances: {dist_single_values4}")
        print("JS-train-divergence: {:.3f}".format( divergence_class4_train ) )
        print("JS-train-distance: {:.3f}".format( distance_class4_train ) )
        print(f"Single train distances: {dist_single_values4_train}")

def get_noise(n_samples, z_dim=8):
    """ Functions that returns n_samples from a standard normal distribution of dimension z_dim """
    return torch.randn(n_samples, z_dim).float()

def generate_n_valid_samples(model, dataset, n_target, n_iter=2500, n_max_iter=-1, z_dim=8, class_label = 0, unique=False, device="cpu"):
      """ Helper function to iteratively generate valid samples """  
     
      device = torch.device(device)

      n_classes = dataset.n_classes
      generated_valid = []
      generated_invalid = []
      generated_all = []
      i = 0 

      failed = False 
      count_generated = 0
      while count_generated < n_target - n_iter:
          if i%100000==0 and i>0: print(i)
          fake_noise = get_noise(n_iter, z_dim=z_dim)
          category = torch.nn.functional.one_hot(class_label*\
                                                  torch.ones(n_iter).to(torch.int64),
                                                  n_classes).float()
          fake = torch.cat( ( fake_noise, category ), 1 ).to(device)
          fake_gen = model( fake ).cpu().detach().numpy()
          fake_gen = np.int32( np.around( [ dataset.unscale( x.reshape(4,2) ) for x in fake_gen ] ) ) 
          for x in fake_gen:
              if dataset._check_valid(x):
                  generated_valid.append(dataset.convert(x,'descriptor','chemical_formula'))
              else:
                  generated_invalid.append(x)
              generated_all.append(x)
          i+=1
          if unique == True:
              generated_valid_unique = set(generated_valid)
              count_generated = len(generated_valid_unique)
          else:
              count_generated = len(generated_valid)
          
          if n_max_iter > -1 and i > n_max_iter:
              failed = True
              break

      if failed:
          return None, None, None

      while not count_generated == n_target:
          fake_noise = get_noise(1, z_dim=z_dim)
          category = torch.nn.functional.one_hot(class_label*\
                                                  torch.ones(1).to(torch.int64),
                                                  n_classes).float()
          fake = torch.cat( ( fake_noise, category ), 1 ).to(device)
          fake_gen = model( fake ).cpu().detach().numpy()
          fake_gen = np.int32( np.around( [ dataset.unscale( x.reshape(4,2) ) for x in fake_gen ] ) )
          for x in fake_gen:
              if dataset._check_valid(x):
                  generated_valid.append(dataset.convert(x,'descriptor','chemical_formula'))
              else:
                  generated_invalid.append(x)
              generated_all.append(x)
          if unique == True:
              generated_valid_unique = set(generated_valid)
              count_generated = len(generated_valid_unique)
          else:
              count_generated = len(generated_valid)
      if unique==True:
          generated_valid=generated_valid_unique

      print(f"\nValid samples: {len(generated_valid)}")
      print(f"Generated samples: {len(generated_all)}")
      print(f"Invalid samples: {len(generated_invalid)}, Fraction: {len(generated_invalid)/len(generated_all)}")
      
      return generated_valid, generated_invalid, generated_all



