mkdir paper_data
wget https://journals.aps.org/prl/supplemental/10.1103/PhysRevLett.117.135502/allEnergies.txt
mv allEnergies.txt paper_data
wget https://journals.aps.org/prl/supplemental/10.1103/PhysRevLett.117.135502/TrainingSet.pkl
mv TrainingSet.pkl paper_data
