# Assessing deep generative models in chemical composition space

In the [article](FILL) we assessed the performance of three deep generative machine learning frameworks (VAE, GAN and RL) for the exploration of a large chemical composition space. 

<img align="center" src="toc_git.png" alt="Overview">

The models were built from simple neural network architectures. To assess their performance, we relied on the closed, fully enumerated chemical composition space of ~ 2 million elpasolite minerals, see [Faber et al.](https://dx.doi.org/10.1103/PhysRevLett.117.135502) for a full description of the dataset.

The repository allows to reproduce the main results presented in the article, thus including the source code for all models, figures, as well as a common dataloader which implements access to the preprocessed dataset and the evaluation metrics.

Models (with final settings described in the article) can be fitted and evaluated by invoking

``` 
python GAN.py
python VAE.py
python RL.py
```

Each model also implements a command-line interface through which non-default settings can be suppled. Run `python <script.py> --help` to access the list and documentation of each option.

Main dependencies are numpy, torch, pandas and need to be installed before.

Note also, that we cannot directly supply the elpasolite dataset. Necessary files can be downloaded by executing `./download_data.sh`
